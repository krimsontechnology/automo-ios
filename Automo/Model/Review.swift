//
//  Review.swift
//  Automo
//
//  Created by OG Eric on 5/2/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import Foundation
import ObjectMapper

class Review: NSObject, Mappable {
    
    var reviewId: String?
    var reviewUserName: String?
    var reviewUserImageUrl: String?
    var reviewRating: Int?
    var reviewText: String?
    
    required init?(map: Map) {}
    
    override init() {
        super.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        reviewId <- map["id"]
        reviewUserName <- map["name"]
        reviewUserImageUrl <- map["image"]
        reviewRating <- map["rating"]
        reviewText <- map["text"]
    }
}
