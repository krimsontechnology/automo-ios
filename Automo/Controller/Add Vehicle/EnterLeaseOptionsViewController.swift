//
//  EnterLeaseOptionsViewController.swift
//  Automo
//
//  Created by OG Eric on 4/24/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

class EnterLeaseOptionsViewController: UIViewController {

    @IBOutlet weak var monthlyRateTextField: UITextField! // 0
    @IBOutlet weak var depositTextField: UITextField! // 1
    @IBOutlet weak var mileageTextField: UITextField! // 2
    @IBOutlet weak var termTextField: UITextField! // 3
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var continueButton: UIButton!
    
    let pickerView = UIPickerView()
    var car = Car()
    
    var textFieldIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.delegate = self
        pickerView.dataSource = self

        monthlyRateTextField.inputView = pickerView
        monthlyRateTextField.delegate = self
        depositTextField.inputView = pickerView
        depositTextField.delegate = self
        mileageTextField.inputView = pickerView
        mileageTextField.delegate = self
        termTextField.inputView = pickerView
        termTextField.delegate = self
        
        self.updateAllFieldsForEditing()
        // Do any additional setup after loading the view.
    }
    
    func updateAllFieldsForEditing() {
        
        self.descriptionTextView.text = self.car.carDescription
        self.depositTextField.text = "$\(self.car.carDeposit)"
        self.monthlyRateTextField.text = "$\(self.car.carPrice)"
        self.termTextField.text = "\(self.car.carTerm) months"
        self.mileageTextField.text = "\(self.car.carMilageAllowance)"
        
        self.checkForCompletion()
    }

    func checkForCompletion() {
        guard monthlyRateTextField.text != "" && depositTextField.text != "" && mileageTextField.text != "" &&
            termTextField.text != "" else {
                
                self.continueButton.isEnabled = false
                self.continueButton.alpha = 1.0
                return
        }
        
        self.continueButton.isEnabled = true
        self.continueButton.alpha = 1.0
    }
    
    @IBAction func becomeFirst(sender: UIButton) {
        switch sender.tag {
        case 0:
            self.monthlyRateTextField.becomeFirstResponder()
        case 1:
            self.depositTextField.becomeFirstResponder()
        case 2:
            self.mileageTextField.becomeFirstResponder()
        case 3:
            self.termTextField.becomeFirstResponder()
        default:
            return
        }
    }
    
    @IBAction func backButton(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func postLease(sender: UIButton) {
        
        self.car.carActive = true
        self.car.carDescription = self.descriptionTextView.text
        
        self.car.carDeposit = Int(self.depositTextField.text?.replacingOccurrences(of: "$", with: "") ?? "0")!
        self.car.carPrice = Int(self.monthlyRateTextField.text?.replacingOccurrences(of: "$", with: "") ?? "0")!
        self.car.carTerm = Int(self.termTextField.text?.components(separatedBy: " ").first ?? "0")!
        self.car.carMilageAllowance = Int(self.mileageTextField.text ?? "0")!
        self.car.carName = self.configCarName(car: self.car)
        
        ref.child(DATABASE_AUTOS).child(self.car.carId ?? "unknown").updateChildValues(self.car.toJSON())
        
        self.createSubscriptionPlan { (done) in
            if done {
                self.navigationController?.popToRootViewController(animated: true)
                NotificationCenter.default.post(name: NSNotification.Name.init("postLease"), object: nil)
            } else {
                // Tell the user that there was an issue and should post again
                
            }
        }
    }
    
    func createSubscriptionPlan(completionHandler: @escaping (Bool) -> Void) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer \(AppDelegate.stripeKey)",
            "Content-Type": "application/x-www-form-urlencoded"]
        let url = "https://api.stripe.com/v1/plans"
        let parameters: Parameters = ["amount": car.carPrice * 100, "currency": "usd", "interval": "month", "id": car.carId!, "product": ["name": car.carName!]]
        
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            debugPrint(response)
            
            switch response.result {
            case .success(_):
                if !(response.response!.statusCode > 399) {
                    completionHandler(true)
                }
            case .failure(_):
                completionHandler(false)
            }
        })
    }
}

extension EnterLeaseOptionsViewController: UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.checkForCompletion()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.textFieldIndex = textField.tag
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch textFieldIndex - 4 {
        case 0:
            return 100
        case 1:
            return 20
        case 2:
            return 20
        case 3:
            return 12
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch textFieldIndex - 4 {
        case 0:
            return "$\(50 * (row + 1))"
        case 1:
            return "$\(250 * (row + 1))"
        case 2:
            return "\(500 * (row + 1))"
        case 3:
            if row == 0 {
               return "\(row + 1) Month"
            } else {
               return "\(row + 1) Months"
            }
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch textFieldIndex - 4 {
        case 0:
            self.monthlyRateTextField.text = "$\(50 * (row + 1))"
        case 1:
            self.depositTextField.text = "$\(250 * (row + 1))"
        case 2:
            self.mileageTextField.text = "\(500 * (row + 1))"
        case 3:
            if row == 0 {
                self.termTextField.text = "\(row + 1) Month"
            } else {
                self.termTextField.text = "\(row + 1) Months"
            }
        default:
            return
        }
    }
}
