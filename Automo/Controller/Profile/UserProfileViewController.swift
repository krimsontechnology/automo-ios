//
//  UserProfileViewController.swift
//  Automo
//
//  Created by OG Eric on 4/14/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Firebase

class UserProfileViewController: ButtonBarPagerTabStripViewController {
    
    static var profileUser = User()

    override func viewDidLoad() {
        
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = .white
        settings.style.selectedBarHeight = 2.0
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func goToSettings(sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        if UserProfileViewController.profileUser.userId == nil { UserProfileViewController.profileUser = MAIN_USER }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileTableViewController") as! ProfileTableViewController
        vc.pageTitle = "Garage"
        let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "ProfileTableViewController") as! ProfileTableViewController
        vc1.pageTitle = "Reviews"
        let vc3 = self.storyboard?.instantiateViewController(withIdentifier: "ProfileTableViewController") as! ProfileTableViewController
        vc3.pageTitle = "Favorites"
        
        return [vc, vc3, vc1]
    }

}
