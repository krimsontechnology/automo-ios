//
//  Networking.swift
//  Automo
//
//  Created by OG Eric on 4/17/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import Stripe

class Networking {
    
    static var baseURL: String = "https://www.carqueryapi.com/api/0.3/?callback=?&cmd="
    
    // Functions used for querying Car Data
    class func getCarFromUrl(modelId: String, completionHandler: @escaping (CarSpecifications?) -> Void) {
        AF.request(Networking.baseURL + "getModel&model=\(modelId)").responseString { (specs) in
            if specs.error == nil {
                do {
                    let value = try specs.result.get()
                    let components = Networking.parseToComponents(string: value)
                    
                    if let comp = components.first, let makeObject = (comp + "}").toJSON() {
                        completionHandler(CarSpecifications(JSON: makeObject)!)
                    }
                } catch {
                    //
                }
            } else {
                completionHandler(nil)
            }
        }
    }
    
    class func getCarMakes(year: Int, completionHandler: @escaping ([CarMake]?) -> Void) {
        AF.request(Networking.baseURL + "getMakes&year=\(year)&sold_in_us=1").responseString { (makes) in
            if makes.error == nil {
                do {
                    let value = try makes.result.get()
                    let components = Networking.parseToComponents(string: value)
                    var carMakes = [CarMake]()
                    
                    // Turn strings in to dictionaries
                    
                    for comp in components {
                        if let makeObject = (comp + "}").toJSON() {
                            carMakes.append(CarMake(JSON: makeObject)!)
                        }
                    }
                    
                    completionHandler(carMakes)
                } catch {
                    
                }
            } else {
                completionHandler(nil)
            }
        }
    }
    
    class func getCarModels(year: Int, make: String, completionHandler: @escaping ([CarModel]?) -> Void) {
        AF.request(Networking.baseURL + "getModels&make=\(make)&year=\(year)&sold_in_us=1").responseString { (models) in
            if models.error == nil {
                
                do {
                    let value = try models.result.get()
                    let components = Networking.parseToComponents(string: value)
                    
                    var carModels = [CarModel]()
                    
                    // Turn strings in to dictionaries
                    
                    for comp in components {
                        if let makeObject = (comp + "}").toJSON() {
                            carModels.append(CarModel(JSON: makeObject)!)
                        }
                    }
                    
                    completionHandler(carModels)
                } catch {
                    
                }
            } else {
                print(models.error.debugDescription)
                completionHandler(nil)
            }
        }
            
    }
    
    class func getCarTrims(year: Int, make: String, model: String, completionHandler: @escaping ([CarTrims]?) -> Void) {
        AF.request(Networking.baseURL + "getTrims&make=\(make)&year=\(year)&model=\(model)&sold_in_us=1").responseString { (trims) in
            if trims.error == nil {
                
                do {
                    let value = try trims.result.get()
                    let components = Networking.parseToComponents(string: value)
                    var carTrims = [CarTrims]()
                    
                    // Turn strings in to dictionaries
                    
                    for comp in components {
                        print(comp)

                        if let makeObject = (comp + "}").toJSON() {
                            let trim = CarTrims(JSON: makeObject)!
                            if trim.displayName != nil && trim.displayName != "" { carTrims.append(trim) }
                        }
                    }
                    
                    completionHandler(carTrims)
                } catch {
                    
                }
                
            } else {
                print(trims.error.debugDescription)
                completionHandler(nil)
            }
        }
    }
    
    class func parseToComponents(string: String) -> [String] {
        let newDict = string.replacingOccurrences(of: "\\", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: ";", with: "").replacingOccurrences(of: "}]}", with: "")
        
        let components = newDict.components(separatedBy: ":[").last!.components(separatedBy: "},")
        return components
    }
    
    // Stripe API calls
    class func createStripeCustomer(completionHandler: @escaping (Bool) -> Void) {
        
        let url = "https://api.stripe.com/v1/customers"
        let headers: HTTPHeaders = ["Authorization": "Bearer \(AppDelegate.stripeKey)"]
        let parameters: Parameters = ["email": MAIN_USER.userEmail ?? ""]
        
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            debugPrint(response)
            
            switch response.result {
            case .success:
                if !(response.response!.statusCode > 399) {
                    do {
                        let dict = try response.result.get() as! [String:Any]
                        if let custId = dict["id"] as? String {
                            // Save Stripe Customer Id to User and Move On
                            ref.child(DATABASE_USER).child(MAIN_USER.userId!).updateChildValues(["customerId": custId])
                            completionHandler(true)
                        }
                    } catch {
                        
                    }
                }
            case .failure:
                completionHandler(false)
            }
        })
    }
    
    class func addCardToCustomer(token: String, completionHandler: @escaping (Bool) -> Void) {
        
        let url = "https://api.stripe.com/v1/customers/\(MAIN_USER.userCustId ?? "")/sources"
        let headers: HTTPHeaders = ["Authorization": "Bearer \(AppDelegate.stripeKey)"]
        let parameters: Parameters = ["source": token]
        
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            
            switch response.result {
            case .success:
                if !(response.response!.statusCode > 399) {
                    completionHandler(true)
                }
            case .failure:
                completionHandler(false)
            }
        })
    }
    
    class func createTokenForCard(cardNumber: String, params: STPPaymentMethodCardParams,completionHandler: @escaping (String?) -> Void) {
        
        let url = "https://api.stripe.com/v1/tokens"
        let headers: HTTPHeaders = ["Authorization": "Bearer \(AppDelegate.stripeKey)"]
        let parameters: Parameters = ["card[number]": cardNumber, "card[exp_month]": params.expMonth as! UInt,
                                                 "card[exp_year]": params.expYear as! UInt, "card[cvc]": params.cvc!]
        
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            
            switch response.result {
            case .success:
                if !(response.response!.statusCode > 399) {
                    do {
                        let dict = try response.result.get() as! [String:Any]
                        if let tokenId = dict["id"] as? String {
                            completionHandler(tokenId)
                        }
                    } catch {
                        
                    }
                }
            case .failure:
                completionHandler(nil)
            }
        })
    }
    
    class func createStripeCustomAccount(userDict: [String:Any], token: String, cardNumber: String, params: STPCardParams, completionHandler:  @escaping (Bool) -> Void) {
        
        let url = "https://api.stripe.com/v1/accounts"
        let headers: HTTPHeaders = ["Authorization": "Bearer \(AppDelegate.stripeKey)"]
        
        // needs to be fixed with proper birthday alignment
        let birthdayComponents = (userDict["birthday"] as! String).components(separatedBy: " ")
        
        let parameters: Parameters = ["legal_entity" : ["type": "individual", "first_name": userDict["firstName"] ,"last_name": userDict["lastName"], "dob" : ["day": Int(birthdayComponents[1])!, "month": Int(birthdayComponents.first!)!, "year": Int(birthdayComponents.last!)!]], "country": "US", "type": "custom", "email": MAIN_USER.userEmail!, "external_account" : token, "tos_acceptance[date]": Int(Date().timeIntervalSince1970), "tos_acceptance[ip]": userDict["ipAddress"] ?? ""]
        
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            debugPrint(response)
            
            
            switch response.result {
            case .success:
                if !(response.response!.statusCode > 399) {
                    do {
                        let dict = try response.result.get() as! [String:Any]
                        if let customId = dict["id"] as? String {
                            MAIN_USER.userStripeToken = customId
                            ref.child("Users").child(MAIN_USER.userId!).updateChildValues(["stripeToken": customId])
                            completionHandler(true)
                        }
                    } catch {
                        
                    }
                }
            case .failure:
                completionHandler(false)
            }
        })
    }
    
    class func getIPAddress(completionHandler: @escaping (String?) -> Void) {
        AF.request("https://api.ipify.org?format=json", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { response in
            debugPrint(response)
            
            switch response.result {
            case .success:
                if !(response.response!.statusCode > 399) {
                    do {
                        let dict = try response.result.get() as! [String:Any]
                        print(dict)
                        completionHandler((dict["ip"] as! String))
                    } catch {
                        
                    }
                }
            case .failure:
                completionHandler(nil)
            }
        })
    }
}
