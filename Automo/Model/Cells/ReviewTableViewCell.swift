//
//  ReviewTableViewCell.swift
//  Automo
//
//  Created by OG Eric on 5/2/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import Cosmos

class ReviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var reviewUserImage: UIImageView!
    @IBOutlet weak var reviewUserName: UILabel!
    @IBOutlet weak var reviewRating: CosmosView!
    @IBOutlet weak var reviewText: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
