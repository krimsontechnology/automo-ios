//
//  Constants.swift
//  Automo
//
//  Created by OG Eric on 4/8/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage
import UIKit
import PopupDialog

var ref: DatabaseReference = Database.database().reference()
var refStorage = Storage.storage().reference().child("carImages")

var DATABASE_USER = "Users"
var DATABASE_AUTOS = "Autos"
var DATABASE_REQUESTS = "Requests"

var MAIN_USER = User()

enum CarCondition: String {
    case new = "New"
    case good = "Good"
    case okay = "Okay"
    case used = "Used"
}

class BorderedButton: UIButton {
    
    override func awakeFromNib() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.white.cgColor
    }
}

extension UIColor {
    static let newPurple = UIColor(red: 143.0/255.0, green: 19.0/255.0, blue: 252.2/255.0, alpha: 1.0)
}

extension String {
    func toJSON() -> [String:Any]?{
        if let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
            } catch {
                print("Something went wrong")
            }
        }
        
        return nil
    }
}

extension UIViewController {
    func configCarName(car: Car) -> String {
        return "\(car.carYear ?? "") \(car.carMake ?? "") \(car.carModel ?? "")"
    }
    
    func changeWheel() {
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "wheel"), object: nil)
    }
    
    func showErrorMessage(title: String, message: String) {
        let popup = PopupDialog(title: title, message: message)
        popup.addButton(PopupDialogButton(title: "Thanks", action: nil))
        UIApplication.topViewController()?.present(popup, animated: true, completion: nil)
    }
}
