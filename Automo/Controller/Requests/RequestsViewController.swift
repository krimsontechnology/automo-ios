//
//  RequestsViewController.swift
//  Automo
//
//  Created by OG Eric on 5/2/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Firebase

class RequestsViewController: UIViewController, IndicatorInfoProvider {
    
    @IBOutlet weak var tableView: UITableView!

    var requests = [Request]()
    var cars = [Car]()
    
    var pageTitle = "Requests"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.downloadUserRequests()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: pageTitle)
    }
    
    func downloadUserRequests() {
        ref.child(DATABASE_REQUESTS).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                self.requests.removeAll()
                
                if let snaps = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snaps {
                        if let dict = snap.value as? [String:Any] {
                            let request = Request(JSON: dict)!
                            
                            if MAIN_USER.userRequests.contains(request.requestId!) {
                                if self.pageTitle.lowercased() == "requests" {
                                    if request.requestStatus != "Leased" {
                                        self.requests.append(request)
                                    }
                                } else {
                                    if request.requestStatus == "Leased" {
                                        self.requests.append(request)
                                    }
                                }
                            }
                        }
                    }
                    
                    self.performCarJoin()
                }
            }
        }
    }
    
    func performCarJoin() {
        
        var keys = [String]()
        
        for request in requests {
            keys.append(request.requestCarId!)
        }
        
        ref.child(DATABASE_AUTOS).observeSingleEvent(of: .value) { (snapshot) in
            if let snaps = snapshot.children.allObjects as? [DataSnapshot] {
                
                for snap in snaps {
                    if let dict = snap.value as? [String:Any] {
                        let car = Car(JSON: dict)!
                        
                        if keys.contains(car.carId ?? "") {
                            self.cars.append(car)
                        }
                    }
                }
                
                self.tableView.reloadData()
            }
        }
    }

}

extension RequestsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let request = requests[indexPath.row]
        let car = cars[indexPath.row]
        
        guard pageTitle != "LEASES" else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RequestBigTableViewCell", for: indexPath) as! RequestTableViewCell
            
            cell.selectionStyle = .none
            cell.requestMainImage.sd_setImage(with: URL(string: car.carFeaturedImage ?? ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            cell.requestCarName.text = request.requestCarName ?? ""
            cell.requestTerm.text = "Remaining Lease Term: \(request.requestLeaseTerm ?? "")"
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            let sentDate = formatter.date(from: request.requestDate!)
            formatter.dateFormat = "MMM d"
            cell.requestDate.text = "Next Payment Date: \(formatter.string(from: sentDate!))"
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestTableViewCell", for: indexPath) as! RequestTableViewCell
        
        cell.selectionStyle = .none
        cell.requestMainImage.sd_setImage(with: URL(string: car.carFeaturedImage ?? ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        cell.requestCarName.text = request.requestCarName ?? ""
        cell.requestTerm.text = "Lease Term: \(request.requestLeaseTerm ?? "")"
        cell.requestStatus.text = "Status: \(request.requestStatus!.uppercased())"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let sentDate = formatter.date(from: request.requestDate!)
        formatter.dateFormat = "MMM d"
        cell.requestDate.text = formatter.string(from: sentDate!)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requests.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestDetailViewController") as! RequestDetailViewController
        vc.request = requests[indexPath.row]
        vc.car = cars[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard pageTitle != "LEASES" else { return 270.0}
        
        return 80.0
    }
}
