//
//  SearchFilterViewController.swift
//  Automo
//
//  Created by OG Eric on 4/11/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage
import SVProgressHUD

class SearchFilterViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var noRecentLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!

    @IBOutlet weak var yearTextField: UITextField!
    @IBOutlet weak var makeTextField: UITextField!
    @IBOutlet weak var modelTextField: UITextField!
    @IBOutlet weak var trimTextField: UITextField!
    @IBOutlet weak var lowPriceTextField: UITextField!
    @IBOutlet weak var highPriceTextField: UITextField!
    @IBOutlet weak var transmissionTextField: UITextField!
    @IBOutlet weak var colorTextField: UITextField!
    
    let pickerView = UIPickerView()
    
    var cars = [Car]()
    
    let transmissions = ["Automatic", "Manual"]
    var makes = [CarMake]()
    var models = [CarModel]()
    var trims = [CarTrims]()
    
    var carModelId: String?
    var textFieldIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        // Add Delegates To All
        
        yearTextField.inputView = pickerView
        yearTextField.delegate = self
        makeTextField.inputView = pickerView
        makeTextField.delegate = self
        modelTextField.inputView = pickerView
        modelTextField.delegate = self
        trimTextField.inputView = pickerView
        trimTextField.delegate = self
        lowPriceTextField.inputView = pickerView
        lowPriceTextField.delegate = self
        highPriceTextField.inputView = pickerView
        highPriceTextField.delegate = self
        transmissionTextField.inputView = pickerView
        transmissionTextField.delegate = self
        
        colorTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.downloadRecentlyViewed()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.yearTextField.text?.removeAll()
//        self.modelTextField.text?.removeAll()
//        self.trimTextField.text?.removeAll()
//        self.makeTextField.text?.removeAll()
//        self.colorTextField.text?.removeAll()
//        self.transmissionTextField.text?.removeAll()
//        self.lowPriceTextField.text?.removeAll()
//        self.highPriceTextField.text?.removeAll()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadRecentlyViewed() {
        ref.child(DATABASE_AUTOS).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                self.cars.removeAll()
                
                if let recents = snapshot.children.allObjects as? [DataSnapshot] {
                    for recent in recents {
                        if let dict = recent.value as? [String:Any] {
                            let car = Car(JSON: dict)!
                            
                            if MAIN_USER.userRecentlyViewed.contains(car.carId ?? "") {
                                self.cars.append(Car(JSON: dict)!)
                            }
                        }
                    }
                    
                    if self.cars.count == 0 {
                        self.collectionViewHeight.constant = 40
                        self.noRecentLabel.isHidden = false
                    } else {
                        self.collectionViewHeight.constant = 142
                        self.noRecentLabel.isHidden = true
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    func loadNextDataPoint(int: Int) {
        SVProgressHUD.show()
        
        switch int {
        case 0:
            guard self.yearTextField.text != "" else { return }
            
            Networking.getCarMakes(year: Int(self.yearTextField.text!)!, completionHandler: { (makes) in
                if makes != nil {
                    self.makes = makes!
                    (self.view.viewWithTag(1) as! UIButton).isEnabled = true
                    self.makeTextField.isEnabled = true
                }
                
                SVProgressHUD.dismiss()
            })
        case 1:
            guard self.yearTextField.text != "" && self.makeTextField.text != "" else { return }
            
            Networking.getCarModels(year: Int(self.yearTextField.text!)!, make: self.makeTextField.text!) { (models) in
                if models != nil {
                    self.models = models!
                    (self.view.viewWithTag(2) as! UIButton).isEnabled = true
                    self.modelTextField.isEnabled = true
                }
                
                SVProgressHUD.dismiss()
            }
        case 2:
            Networking.getCarTrims(year: Int(self.yearTextField.text!)!, make: self.makeTextField.text!, model: self.modelTextField.text!.replacingOccurrences(of: " ", with: "%20")) { (trims) in
                
                if trims != nil {
                    self.trims = trims!
                    (self.view.viewWithTag(3) as! UIButton).backgroundColor = UIColor.clear
                    (self.view.viewWithTag(3) as! UIButton).isEnabled = true
                    self.trimTextField.isEnabled = true
                } else {
                    // IF TRIMS ARE EMPTY THEN DISABLE BUTTON AND GRAY OUT BUTTON
                    (self.view.viewWithTag(3) as! UIButton).backgroundColor = UIColor.black.withAlphaComponent(0.5)
                    (self.view.viewWithTag(3) as! UIButton).isEnabled = false
                    self.trimTextField.isEnabled = false
                }
                
                SVProgressHUD.dismiss()
            }
        default:
            return
        }
    }
    
    @IBAction func becomeFirst(sender: UIButton) {
        switch sender.tag {
        case 0:
            self.yearTextField.becomeFirstResponder()
        case 1:
            self.makeTextField.becomeFirstResponder()
        case 2:
            self.modelTextField.becomeFirstResponder()
        case 3:
            self.trimTextField.becomeFirstResponder()
        case 4:
            self.lowPriceTextField.becomeFirstResponder()
        case 5:
            self.highPriceTextField.becomeFirstResponder()
        case 6:
            self.transmissionTextField.becomeFirstResponder()
        case 7:
            self.colorTextField.becomeFirstResponder()
        default:
            return
        }
    }
    
    @IBAction func showResults(sender: UIButton) {
        
        SVProgressHUD.show()
        
        // find a way to check each of the text fields for data, then use that data to query the Cars database
        // NOTE: 8 if statements are ugly.. but right now I can't think of a better way
        
        // PROBLEM: FIREBASE ONLY ALLOWS BASICALLY SINGULAR QUERIES
        
        var searchAutos = [Car]()
        
        if carModelId != nil {
            ref.child(DATABASE_AUTOS).queryOrdered(byChild: "modelId").queryEqual(toValue: self.carModelId!).observeSingleEvent(of: .value) { (snapshot) in
                if let snaps = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snaps {
                        if let dict = snap.value as? [String:Any] {
                            let car = Car(JSON: dict)!
                            // PERFORM CLIENT FILTERING HERE!
                            
                            if self.lowPriceTextField.text != "" {
                                // CHECK IF PRICE IS BELOW LOW PRICE
                                if car.carPrice < Int(self.lowPriceTextField.text!.replacingOccurrences(of: "$", with: ""))! {
                                    continue
                                }
                            }
                            
                            if self.highPriceTextField.text != "" {
                                // CHECK IF PRICE IS ABOVE HIGH PRICE
                                if car.carPrice > Int(self.highPriceTextField.text!.replacingOccurrences(of: "$", with: ""))! {
                                    continue
                                }
                            }
                            
                            if self.transmissionTextField.text != "" {
                                if car.carTransmission != self.transmissionTextField.text ?? "" {
                                    continue
                                }
                            }
                            
                            if self.colorTextField.text != "" {
                                if car.carColor != self.colorTextField.text ?? "" {
                                    continue
                                }
                            }
                            
                            searchAutos.append(Car(JSON: dict)!)
                        }
                    }
                    SVProgressHUD.dismiss()
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
                    vc.cars = searchAutos
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        } else {
            ref.child(DATABASE_AUTOS).observeSingleEvent(of: .value) { (snapshot) in
                if let snaps = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snaps {
                        if let dict = snap.value as? [String:Any] {
                            let car = Car(JSON: dict)!
                            // PERFORM CLIENT FILTERING HERE!
                            
                            if self.yearTextField.text != "" {
                                if car.carYear != self.yearTextField.text {
                                    continue
                                }
                            }
                            
                            if self.makeTextField.text != "" {
                                if car.carMake != self.makeTextField.text {
                                    continue
                                }
                            }
                            
                            if self.modelTextField.text != "" {
                                if car.carModel != self.modelTextField.text {
                                    continue
                                }
                            }
                            
                            if self.trimTextField.text != "" {
                                if car.carTrim != self.trimTextField.text {
                                    continue
                                }
                            }
                            
                            if self.lowPriceTextField.text != "" {
                                // CHECK IF PRICE IS BELOW LOW PRICE
                                if car.carPrice < Int(self.lowPriceTextField.text!.replacingOccurrences(of: "$", with: ""))! {
                                    continue
                                }
                            }
                            
                            if self.highPriceTextField.text != "" {
                                // CHECK IF PRICE IS ABOVE HIGH PRICE
                                if car.carPrice > Int(self.highPriceTextField.text!.replacingOccurrences(of: "$", with: ""))! {
                                    continue
                                }
                            }
                            
                            if self.transmissionTextField.text != "" {
                                if car.carTransmission != self.transmissionTextField.text ?? "" {
                                    continue
                                }
                            }
                            
                            if self.colorTextField.text != "" {
                                if car.carColor != self.colorTextField.text ?? "" {
                                    continue
                                }
                            }
                            
                            searchAutos.append(Car(JSON: dict)!)
                        }
                    }
                    SVProgressHUD.dismiss()
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
                    vc.cars = searchAutos
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }

}

extension SearchFilterViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCollectionViewCell", for: indexPath) as! SearchCollectionViewCell
        
        let thisCar = cars[indexPath.row]
        cell.carImage.sd_setImage(with: URL(string: thisCar.carFeaturedImage ?? ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        cell.carName.text = "\(thisCar.carYear ?? "") \(thisCar.carMake ?? "") \(thisCar.carModel ?? "")"
        cell.monthlyPrice.text = "$\(thisCar.carPrice)/month"
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cars.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = self.collectionView.frame.height
        return CGSize(width: height, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CarDetailViewController") as! CarDetailViewController
        vc.car = cars[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SearchFilterViewController: UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag < 11 {
            self.loadNextDataPoint(int: textField.tag - 8)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.textFieldIndex = textField.tag
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch textFieldIndex - 8 {
        case 0:
            return 69
        case 1:
            return makes.count
        case 2:
            return models.count
        case 3:
            return trims.count
        case 4:
            return 100
        case 5:
            return 100
        case 6:
            return transmissions.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch textFieldIndex - 8 {
        case 0:
            return "\(2018 - row)"
        case 1:
            return makes[row].displayName ?? ""
        case 2:
            return models[row].displayName ?? ""
        case 3:
            return trims[row].displayName ?? ""
        case 4:
            return "$\(50 * (row + 1))"
        case 5:
            return "$\(50 * (row + 1))"
        case 6:
            return transmissions[row]
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch textFieldIndex - 8 {
        case 0:
            self.yearTextField.text = "\(2018 - row)"
        case 1:
            self.makeTextField.text = makes[row].displayName ?? ""
        case 2:
            self.modelTextField.text = models[row].displayName ?? ""
        case 3:
            self.trimTextField.text = trims[row].displayName ?? ""
            self.carModelId = trims[row].id ?? ""
        case 4:
            self.lowPriceTextField.text = "$\(50 * (row + 1))"
        case 5:
            self.highPriceTextField.text = "$\(50 * (row + 1))"
        case 6:
            self.transmissionTextField.text = transmissions[row]
        default:
            return
        }
    }
}
