//
//  AddPhotosViewController.swift
//  Automo
//
//  Created by OG Eric on 4/22/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import ImagePicker
import SVProgressHUD

class AddPhotosViewController: UIViewController {
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var imagePickerController: ImagePickerController!
    var carImages = [UIImage]()
    
    var car = Car()
    var firstLoad: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let configuration = Configuration()
        
        configuration.doneButtonTitle = "Continue"
        configuration.noImagesTitle = "Sorry! There are no images here!"
        
        imagePickerController = ImagePickerController(configuration: configuration)
        imagePickerController.imageLimit = 15
        imagePickerController.delegate = self
        
        if car.carImages.count == 0 {
            self.present(imagePickerController, animated: true, completion: nil)
            self.firstLoad = false
        } else {
            self.collectionView.reloadData()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard car.carImages.count == 0 else {
            self.continueButton.isEnabled = true
            self.continueButton.alpha = 1.0
            return
        }
        
        self.continueButton.isEnabled = carImages.count > 0 ? true : false
        self.continueButton.alpha = carImages.count > 0 ? 1.0 : 0.5
    }
    
    @IBAction func reopenCamera(sender: UIButton) {
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func backButton(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func uploadAnImage(x: Int) {
        guard carImages.count > 0 else {
            SVProgressHUD.dismiss()
            self.car.carFeaturedImage = self.car.carImages.first!
            ref.child(DATABASE_AUTOS).child(self.car.carId ?? "unknown").updateChildValues(self.car.toJSON())
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterLeaseOptionsViewController") as! EnterLeaseOptionsViewController
            vc.car = self.car
            return self.navigationController!.pushViewController(vc, animated: true)
        }
        
        let newImage = carImages[x].jpegData(compressionQuality: 0.15)!
        let imageId = ref.childByAutoId().debugDescription.components(separatedBy: "/").last!
        
        refStorage.child(imageId).putData(newImage, metadata: nil) { (metadata, error) in
            guard let _ = metadata else {
                // Uh-oh, an error occurred!
                SVProgressHUD.dismiss()
                return
            }
            
            refStorage.child(imageId).downloadURL(completion: { (url, error) in
                if let url = url {
                    self.car.carImages.append(url.absoluteString)
                    
                    let newX = x + 1
                    
                    if x == self.carImages.count - 1 {
                        SVProgressHUD.dismiss()
                        
                        self.car.carFeaturedImage = self.car.carImages.first!
                        
                        ref.child(DATABASE_AUTOS).child(self.car.carId ?? "unknown").updateChildValues(self.car.toJSON())
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterLeaseOptionsViewController") as! EnterLeaseOptionsViewController
                        vc.car = self.car
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        return self.uploadAnImage(x: newX)
                    }
                }
            })
        }
    }
    
    @IBAction func uploadImages(sender: UIButton) {
        SVProgressHUD.show()
        self.uploadAnImage(x: 0)
    }
    
    @objc func removePhoto(sender: UIButton) {
        if car.carImages.count > 0 {
            if sender.tag >= car.carImages.count {
                self.carImages.remove(at: sender.tag - car.carImages.count)
            } else {
                self.car.carImages.remove(at: sender.tag)
            }
        } else {
            self.carImages.remove(at: sender.tag)
        }
        
        self.collectionView.reloadData()
    }
    
}

extension AddPhotosViewController: ImagePickerDelegate {
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {}
    
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.carImages = images
        self.collectionView.reloadData()
        imagePickerController.dismiss(animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        self.imagePickerController.dismiss(animated: true) {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension AddPhotosViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCollectionViewCell", for: indexPath) as! SearchCollectionViewCell
        
        if car.carImages.count > indexPath.row {
            cell.carImage.sd_setImage(with: URL(string: car.carImages[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        } else {
            cell.carImage.image = carImages[indexPath.row - car.carImages.count]
        }
        
        cell.removeButton.tag = indexPath.row
        cell.removeButton.addTarget(self, action: #selector(removePhoto(sender:)), for: .touchUpInside)
        
        if firstLoad && indexPath.row + 1 == car.carImages.count {
            firstLoad = false
        }
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return carImages.count + car.carImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.collectionView.frame.width
        return CGSize(width: width / 3.05, height: width / 3.05)
    }
}
