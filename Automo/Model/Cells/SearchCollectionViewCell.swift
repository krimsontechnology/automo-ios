//
//  SearchCollectionViewCell.swift
//  Automo
//
//  Created by OG Eric on 4/10/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit

class SearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var monthlyPrice: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    
    override func awakeFromNib() {}
}
