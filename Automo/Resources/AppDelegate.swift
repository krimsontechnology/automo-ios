//
//  AppDelegate.swift
//  Automo
//
//  Created by OG Eric on 4/9/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import SVProgressHUD
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    var window: UIWindow?
    static let stripeKey = "sk_test_7RDC2ya1wRWAVyl33UeX67AT"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.statusBarStyle = .lightContent
        if UIWindow().frame.height < 810.0 { UIApplication.shared.isStatusBarHidden = true }
        
        FirebaseApp.configure()
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        IQKeyboardManager.shared.enable = true
        SVProgressHUD.setMaximumDismissTimeInterval(2.0)
        
        return true
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        GIDSignIn.sharedInstance()?.handle(url)
        ApplicationDelegate.shared.application(application, open: url, options: options)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

extension UIApplication {
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

extension AppDelegate {
    
    // Used for Connecting Google
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if error != nil {
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (user, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }
            
            SVProgressHUD.show()
            ref.child(DATABASE_USER).child(user!.user.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    if let mainUser = snapshot.value as? [String:Any] {
                        MAIN_USER = User(JSON: mainUser)!
                    }
                } else {
                    let dict = ["id": user!.user.uid, "email": user!.user.email ?? "", "name": user!.user.displayName ?? "", "image": user!.user.photoURL?.absoluteString ?? ""]
                    MAIN_USER = User(JSON: dict)!
                    ref.child(DATABASE_USER).child(user!.user.uid).updateChildValues(["id": user!.user.uid, "email": user!.user.email ?? "", "name": user!.user.displayName ?? "", "image": user!.user.photoURL?.absoluteString ?? ""])
                }
                
                SVProgressHUD.dismiss()
                print("Logged In With Google")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "google"), object: nil)
            })
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
    }
}

