//
//  ViewController.swift
//  Automo
//
//  Created by OG Eric on 4/4/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKLoginKit
import SwiftyGif
import Hero
import SVProgressHUD

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var connectGoogle: GIDSignInButton!
    @IBOutlet weak var connectFacebook: UIButton!
    @IBOutlet weak var backgroundGIF: UIImageView!
    
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        connectGoogle.layer.borderColor = UIColor.white.cgColor
        connectFacebook.layer.borderColor = UIColor.white.cgColor
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        NotificationCenter.default.addObserver(self, selector: #selector(proceedToMainView), name: NSNotification.Name(rawValue: "google"), object: nil)
        
        do {
            let gif = try UIImage(gifName: "autonomous-intersection")
            backgroundGIF.setGifImage(gif)
            backgroundGIF.loopCount = -1
            backgroundGIF.startAnimating()
        } catch {
            print(error)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let id = self.defaults.value(forKey: "userId") as? String {
            self.grabUser(id: id)
        }
    }
    
    @objc func proceedToMainView() {
        if MAIN_USER.userCustId == nil {
            Networking.createStripeCustomer { (done) in
                let nav = self.storyboard?.instantiateViewController(withIdentifier: "MainTabController") as! UITabBarController
                self.present(nav, animated: true, completion: nil)
            }
        } else {
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "MainTabController") as! UITabBarController
            self.present(nav, animated: true, completion: nil)
        }
        
        self.defaults.set(MAIN_USER.userId, forKey: "userId")
    }
    
    @IBAction func connectGoogle(sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func connectFacebook(sender: UIButton) {
        let manager = LoginManager()
        manager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            
            if error != nil {
                return
            }
            
            guard let token = AccessToken.current?.tokenString else { return }
            let credential = FacebookAuthProvider.credential(withAccessToken: token)
            Auth.auth().signIn(with: credential) { (user, error) in
                if error != nil {
                    return
                }
                
                self.grabUser(for: user)
            }
        }
    }
    
    func grabUser(for user: AuthDataResult? = nil, id: String? = "") {
//        SVProgressHUD.show()
        let userId = user != nil ? user!.user.uid : id!
        ref.child(DATABASE_USER).child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                if let mainUser = snapshot.value as? [String:Any] {
                    MAIN_USER = User(JSON: mainUser)!
                }
            } else {
                if let user = user?.user {
                    MAIN_USER = User(JSON: ["id": user.uid, "email": user.email ?? "",
                                            "name": user.displayName ?? "", "image": user.photoURL?.absoluteString ?? ""])!
                    ref.child(DATABASE_USER).child(user.uid).updateChildValues(["id": user.uid, "email": user.email ?? "", "name": user.displayName ?? "", "image": user.photoURL?.absoluteString ?? ""])
                }
            }
            
            SVProgressHUD.dismiss()
            print("Logged In With Facebook")
            self.proceedToMainView()
        })
    }
}

