//
//  request.swift
//  Automo
//
//  Created by OG Eric on 5/2/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import Foundation
import ObjectMapper

class Request: NSObject, Mappable {
    
    var requestId: String?
    var requestCarId: String?
    var requestSenderId: String?
    var requestOwnerId: String?
    var requestCarName: String?
    var requestLeaseTerm: String?
    var requestLeaseStartDate: String?
    var requestDate: String?
    var requestStatus: String?
    
    var requestOwner = User()
    var requestSender = User()
    
    required init?(map: Map) {}
    
    override init() {
        super.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        requestId <- map["id"]
        requestSenderId <- map["senderId"]
        requestOwnerId <- map["ownerId"]
        requestCarName <- map["carName"]
        requestCarId <- map["carId"]
        requestLeaseTerm <- map["term"]
        requestLeaseStartDate <- map["startDate"]
        requestDate <- map["date"]
        requestStatus <- map["status"]
    }
}
