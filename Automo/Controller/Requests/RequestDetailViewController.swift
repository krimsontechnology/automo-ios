//
//  RequestDetailViewController.swift
//  Automo
//
//  Created by OG Eric on 5/2/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import PopupDialog
import Firebase
import Alamofire

class RequestDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var carTrim: UILabel!
    @IBOutlet weak var carMiles: UILabel!
    
    @IBOutlet weak var leaseStartDate: UILabel!
    @IBOutlet weak var leaseEndDate: UILabel!
    @IBOutlet weak var leaseTerm: UILabel!
    @IBOutlet weak var totalMileage: UILabel!
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var denyButton: UIButton!
    
    var request = Request()
    var car = Car()
    
    var leaseMonths: Int = 0
    var startDate = Date()
    var formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if request.requestSenderId == MAIN_USER.userId {
            denyButton.removeFromSuperview()
            acceptButton.setTitle("CANCEL REQUEST", for: .normal)

            acceptButton.backgroundColor = .white
            acceptButton.setTitleColor(.black, for: .normal)
            acceptButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16.0)

            acceptButton.addTarget(self, action: #selector(cancelRequest(sender:)), for: .touchUpInside)

            if request.requestStatus != "Pending" {
                acceptButton.isEnabled = false
                acceptButton.alpha = 0.5
            }
        } else {
            acceptButton.addTarget(self, action: #selector(acceptRequest(sender:)), for: .touchUpInside)
        }
    
        // Do any additional setup after loading the view.
        self.carImage.sd_setImage(with: URL(string: car.carFeaturedImage ?? ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        self.carTrim.text = "Car Trim: \(self.car.carTrim ?? "")"
        self.carName.text = self.configCarName(car: self.car)
        self.carMiles.text = "Car Odometer: \(self.car.carMilage) miles"
        
        leaseMonths = Int(self.request.requestLeaseTerm!.components(separatedBy: " ").first!)!
        self.viewHeight.constant = CGFloat(350 + (leaseMonths * 40))
            
        self.leaseTerm.text = self.request.requestLeaseTerm ?? "0 months"
        self.totalMileage.text = "\(self.car.carMilageAllowance * leaseMonths) miles"
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        startDate = formatter.date(from: request.requestLeaseStartDate!)!
        formatter.dateFormat = "MMM d, yyyy"
        self.leaseStartDate.text = formatter.string(from: startDate)
        let endDate = Calendar.current.date(byAdding: Calendar.Component.month, value: leaseMonths, to: startDate)!
        self.leaseEndDate.text = formatter.string(from: endDate)
        
        self.tableView.tableFooterView = UIView()
        
        self.tableView.reloadData()
        self.downloadOwnerInfo()
    }
    
    func downloadOwnerInfo() {
        ref.child(DATABASE_USER).child(car.ownerId ?? "Unknown").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                if let thisUser = snapshot.value as? [String:Any] {
                    self.car.owner = User(JSON: thisUser)!
                }
            }
        })
        
        ref.child(DATABASE_USER).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                if let snaps = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snaps {
                        if let dict = snap.value as? [String: Any] {
                            
                            let user = User(JSON: dict)!
                            
                            if self.request.requestSenderId == user.userId {
                                self.request.requestSender = user
                            } else if self.request.requestOwnerId == user.userId {
                                self.request.requestOwner = user
                            }
                        }
                    }
                }
            }
        }
    }
    
    // TODO: FIGURE OUT HOW TO CHANGE PAYMENT DATE FOR LEASES IN ADVANCE?
    
    func addToSubscription(planId: String, completionHandler: @escaping (Bool) -> Void) {
        
        let headers: HTTPHeaders = ["Authorization": "Bearer \(AppDelegate.stripeKey)",
            "Content-Type": "application/x-www-form-urlencoded"]
        let url = "https://api.stripe.com/v1/subscriptions"
        let parameters: Parameters = ["customer": request.requestSender.userCustId!, "items" : ["0": ["plan": planId]]]
        
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            debugPrint(response)
            switch response.result {
            case .success(let json):
                if !(response.response!.statusCode > 399) {
                    // Handle Successful Creation Here
                    completionHandler(true)
                }
            case .failure(let error):
                // Handle Failed Payment Here
                completionHandler(false)
            }
        })
    }
    
    //WHY: Subscription handles the initial payment upon sign up
    
//    func chargeUserForFirstPayment() {
//
//        let headers: HTTPHeaders = ["Authorization": "Bearer \(AppDelegate.stripeKey)",
//            "Content-Type": "application/x-www-form-urlencoded"]
//        let url = "https://api.stripe.com/v1/charges"
//        let parameters: Parameters = ["amount": car.carPrice * 100, "currency": "usd", "customer": request.requestSender.userCustId!, "description": "First payment for lease id: \(request.requestId!)"]
//
//        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON(completionHandler: { response in
//            debugPrint(response)
//
//            if !(response.result.isFailure) && !(response.response!.statusCode > 399) {
//                // Handle Successful Payment Here
//            } else {
//                // Handle Failed Payment Here
//            }
//        })
//    }
    
    @objc func acceptRequest(sender: UIButton) {
        // CHARGE THE CUSTOMER HERE
        self.addToSubscription(planId: car.carId!) { (subscribed) in
            if subscribed {
                ref.child(DATABASE_REQUESTS).child(self.request.requestId!).updateChildValues(["status": "Leased"])
                ref.child(DATABASE_AUTOS).child(self.car.carId!).updateChildValues(["leased": true])
                
                //TODO: Send users instructional email about lease
                
                self.navigationController?.popViewController(animated: true)
            } else {
                //TODO: Show error message that stripe had an error
            }
        }
    }
    
    @IBAction func denyRequest(sender: UIButton) {
        // Update these options later
        
        let popup = PopupDialog(title: "Deny Lease Request", message: "Please choose an option to tell us why you are choosing to deny this request. The user who requested will see this response and we will use this to improve our experience in the future.")
        popup.buttonAlignment = .vertical
        let firstOption = PopupDialogButton(title: "Lease Term Not Available") {
            ref.child(DATABASE_REQUESTS).child(self.request.requestId!).updateChildValues(["status": "Denied"])
            self.navigationController?.popViewController(animated: true)
        }
        let secondOption = PopupDialogButton(title: "Vehicle No Longer Available") {
            ref.child(DATABASE_REQUESTS).child(self.request.requestId!).updateChildValues(["status": "Denied"])
            self.navigationController?.popViewController(animated: true)
        }
        let thirdOption = PopupDialogButton(title: "Accepting Different Request") {
            ref.child(DATABASE_REQUESTS).child(self.request.requestId!).updateChildValues(["status": "Denied"])
            self.navigationController?.popViewController(animated: true)
        }
        let cancelButton = PopupDialogButton(title: "Cancel", action: nil)
        popup.addButtons([firstOption, secondOption, thirdOption, cancelButton])
        
        self.present(popup, animated: true, completion: nil)
    }
    
    @objc func cancelRequest(sender: UIButton) {
        
        ref.child(DATABASE_REQUESTS).child(request.requestId!).updateChildValues(["status": "Cancelled"])
        self.navigationController?.popViewController(animated: true)
    }
}

extension RequestDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarDetailTableViewCell", for: indexPath) as! CarDetailTableViewCell

        if leaseMonths == indexPath.row {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: cell.frame.width, height: 1.0))
            view.backgroundColor = .white
            cell.addSubview(view)
            
            cell.carDetails.text = "Lease Total"
            cell.carMonthlyRate.text = "$\(car.carPrice * leaseMonths).00"
        } else {
            let newDate = Calendar.current.date(byAdding: Calendar.Component.month, value: indexPath.row , to: startDate)!
            cell.carDetails.text = formatter.string(from: newDate)
            cell.carMonthlyRate.text = "$\(car.carPrice).00"
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leaseMonths + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
}
