//
//  User.swift
//  Automo
//
//  Created by OG Eric on 4/8/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import Foundation
import ObjectMapper

class User: NSObject, Mappable {
    
    var userId: String?
    var userName: String?
    var userEmail: String?
    var userCustId: String?
    var userStripeToken: String?
    var userImageUrl: String?
    var userGarage: [String] = []
    var userRequests: [String] = []
    var userReviews: [String] = []
    var userFavorites: [String] = []
    var userRecentlyViewed: [String] = []
    
    required init?(map: Map) {}
    
    override init() {
        super.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        userId <- map["id"]
        userName <- map["name"]
        userEmail <- map["email"]
        userCustId <- map["customerId"]
        userStripeToken <- map["stripeToken"]
        userImageUrl <- map["image"]
        userGarage <- map["garage"]
        userRequests <- map["requests"]
        userReviews <- map["reviews"]
        userFavorites <- map["favorites"]
        userRecentlyViewed <- map["recent"]
    }
}
