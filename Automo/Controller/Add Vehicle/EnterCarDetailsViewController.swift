//
//  EnterCarDetailsViewController.swift
//  Automo
//
//  Created by OG Eric on 4/22/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class EnterCarDetailsViewController: UIViewController {
    
    @IBOutlet weak var yearTextField: UITextField! // 0
    @IBOutlet weak var makeTextField: UITextField! // 1
    @IBOutlet weak var modelTextField: UITextField! // 2
    @IBOutlet weak var trimTextField: UITextField! // 3
    @IBOutlet weak var conditionTextField: UITextField! // 4
    @IBOutlet weak var transTextField: UITextField! // 5
    
    @IBOutlet weak var mileageTextField: UITextField!
    @IBOutlet weak var colorTextField: UITextField!
    
    @IBOutlet weak var continueButton: UIButton!

    let pickerView = UIPickerView()
    
    let transmissions = ["Automatic", "Manual"]
    let conditions = ["Excellent", "Fair", "Okay", "Damaged"]
    var makes = [CarMake]()
    var models = [CarModel]()
    var trims = [CarTrims]()
    
    var carModelId: String?
    var textFieldIndex: Int = 0
    
    static var currentCar = Car()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.delegate = self
        pickerView.dataSource = self

        // Add Delegates To All
        
        yearTextField.inputView = pickerView
        yearTextField.delegate = self
        makeTextField.inputView = pickerView
        makeTextField.delegate = self
        modelTextField.inputView = pickerView
        modelTextField.delegate = self
        trimTextField.inputView = pickerView
        trimTextField.delegate = self
        transTextField.inputView = pickerView
        transTextField.delegate = self
        conditionTextField.inputView = pickerView
        conditionTextField.delegate = self
        
        mileageTextField.delegate = self
        colorTextField.delegate = self
        
        self.updateAllFieldsForEditing()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc func postedLease() {
        self.backToApp(sender: UIButton())
    }
    
    func updateAllFieldsForEditing() {
        guard EnterCarDetailsViewController.currentCar.carId != nil else { return }
        
        self.carModelId = EnterCarDetailsViewController.currentCar.carModelId ?? ""
        self.makeTextField.text = EnterCarDetailsViewController.currentCar.carMake ?? ""
        self.modelTextField.text = EnterCarDetailsViewController.currentCar.carModel ?? ""
        self.yearTextField.text = EnterCarDetailsViewController.currentCar.carYear ?? ""
        self.trimTextField.text = EnterCarDetailsViewController.currentCar.carTrim ?? ""
        self.conditionTextField.text = EnterCarDetailsViewController.currentCar.carCondition ?? ""
        self.transTextField.text = EnterCarDetailsViewController.currentCar.carTransmission ?? ""
        self.mileageTextField.text = "\(EnterCarDetailsViewController.currentCar.carMilage)"
        self.colorTextField.text = EnterCarDetailsViewController.currentCar.carColor ?? ""
        
        self.checkForCompletion()
    }
    
    func checkForCompletion() {
        guard makeTextField.text != "" && modelTextField.text != "" && yearTextField.text != "" &&
            conditionTextField.text != "" && transTextField.text != "" && mileageTextField.text != "" &&
        colorTextField.text != "" else {
            
            self.continueButton.isEnabled = false
            self.continueButton.alpha = 0.5
            return
        }
        
        self.continueButton.isEnabled = true
        self.continueButton.alpha = 1.0
    }
    
    @IBAction func becomeFirst(sender: UIButton) {
        switch sender.tag {
        case 0:
            self.yearTextField.becomeFirstResponder()
        case 1:
            self.makeTextField.becomeFirstResponder()
        case 2:
            self.modelTextField.becomeFirstResponder()
        case 3:
            self.trimTextField.becomeFirstResponder()
        case 4:
            self.conditionTextField.becomeFirstResponder()
        case 5:
            self.mileageTextField.becomeFirstResponder()
        case 6:
            self.transTextField.becomeFirstResponder()
        case 7:
            self.colorTextField.becomeFirstResponder()
        default:
            return
        }
    }
    
    func loadNextDataPoint(int: Int) {
        SVProgressHUD.show()
        
        switch int {
        case 0:
            guard self.yearTextField.text != "" else { return }
            
            //Clear other views
            self.makeTextField.text?.removeAll()
            self.modelTextField.text?.removeAll()
            self.trimTextField.text?.removeAll()
            self.checkForCompletion()
            
            Networking.getCarMakes(year: Int(self.yearTextField.text!)!, completionHandler: { (makes) in
                if makes != nil {
                    self.makes = makes!
                    (self.view.viewWithTag(1) as! UIButton).isEnabled = true
                    self.makeTextField.isEnabled = true
                }
                
                SVProgressHUD.dismiss()
            })
        case 1:
            guard self.yearTextField.text != "" && self.makeTextField.text != "" else { return }
            
            self.modelTextField.text?.removeAll()
            self.trimTextField.text?.removeAll()
            self.checkForCompletion()
            
            Networking.getCarModels(year: Int(self.yearTextField.text!)!, make: self.makeTextField.text!) { (models) in
                if models != nil {
                    self.models = models!
                    (self.view.viewWithTag(2) as! UIButton).isEnabled = true
                    self.modelTextField.isEnabled = true
                }
                
                SVProgressHUD.dismiss()
            }
        case 2:
            guard self.yearTextField.text != "" && self.makeTextField.text != "" && self.modelTextField.text != "" else { return }
            
            self.trimTextField.text?.removeAll()
            self.checkForCompletion()
            
            Networking.getCarTrims(year: Int(self.yearTextField.text!)!, make: self.makeTextField.text!, model: self.modelTextField.text!.replacingOccurrences(of: " ", with: "%20")) { (trims) in
                
                if trims != nil {
                    self.trims = trims!
                    (self.view.viewWithTag(3) as! UIButton).isEnabled = true
                    (self.view.viewWithTag(3) as! UIButton).backgroundColor = UIColor.clear
                    self.trimTextField.isEnabled = true
                } else {
                    // IF TRIMS ARE EMPTY THEN DISABLE BUTTON AND GRAY OUT BUTTON
                    (self.view.viewWithTag(3) as! UIButton).backgroundColor = UIColor.black.withAlphaComponent(0.5)
                    (self.view.viewWithTag(3) as! UIButton).isEnabled = false
                    self.trimTextField.isEnabled = false
                }
                
                SVProgressHUD.dismiss()
            }
        default:
            return
        }
    }
    
    @IBAction func continueToPhotos(sender: UIButton) {
        
        let id: String = EnterCarDetailsViewController.currentCar.carId != nil ? EnterCarDetailsViewController.currentCar.carId! : ref.childByAutoId().debugDescription.components(separatedBy: "/").last!
        
        var car = Car()
        car = EnterCarDetailsViewController.currentCar
        
        car.carId = id
        car.carActive = EnterCarDetailsViewController.currentCar.carId != nil ? EnterCarDetailsViewController.currentCar.carActive : false
        car.carMake = makeTextField.text
        car.carModel = modelTextField.text
        car.carYear = yearTextField.text
        car.carTrim = trimTextField.text
        car.carCondition = conditionTextField.text
        car.carTransmission = transTextField.text
        car.carMilage = Int(mileageTextField.text ?? "0")!
        car.carColor = colorTextField.text
        car.carModelId = carModelId
        
        ref.child(DATABASE_AUTOS).child(id).updateChildValues(car.toJSON())
        
        if EnterCarDetailsViewController.currentCar.carId == nil {
            ref.child(DATABASE_USER).child(MAIN_USER.userId!).updateChildValues(["garage": [id]])
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPhotosViewController") as! AddPhotosViewController
        vc.car = car
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backToApp(sender: UIButton) {
        
        self.yearTextField.text?.removeAll()
        self.modelTextField.text?.removeAll()
        self.trimTextField.text?.removeAll()
        self.makeTextField.text?.removeAll()
        self.colorTextField.text?.removeAll()
        self.conditionTextField.text?.removeAll()
        self.mileageTextField.text?.removeAll()
        self.transTextField.text?.removeAll()
        
        EnterCarDetailsViewController.currentCar = Car()
        
        self.dismiss(animated: true, completion: nil)
    }

}

extension EnterCarDetailsViewController: UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag < 11 {
            self.loadNextDataPoint(int: textField.tag - 8)
        }
        
        self.checkForCompletion()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.textFieldIndex = textField.tag
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch textFieldIndex - 8 {
        case 0:
            return 11
        case 1:
            return makes.count
        case 2:
            return models.count
        case 3:
            return trims.count
        case 4:
            return conditions.count
        case 5:
            return transmissions.count
        default:
            return 0
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch textFieldIndex - 8 {
        case 0:
            return "\(2018 - row)"
        case 1:
            return makes[row].displayName ?? ""
        case 2:
            return models[row].displayName ?? ""
        case 3:
            return trims[row].displayName ?? ""
        case 4:
            return conditions[row]
        case 5:
            return transmissions[row]
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch textFieldIndex - 8 {
        case 0:
            self.yearTextField.text = "\(2018 - row)"
        case 1:
            self.makeTextField.text = makes[row].displayName ?? ""
        case 2:
            self.modelTextField.text = models[row].displayName ?? ""
        case 3:
            self.trimTextField.text = trims[row].displayName ?? ""
            self.carModelId = trims[row].id ?? ""
        case 4:
            self.conditionTextField.text = conditions[row]
        case 5:
            self.transTextField.text = transmissions[row]
        default:
            return
        }
    }
}
