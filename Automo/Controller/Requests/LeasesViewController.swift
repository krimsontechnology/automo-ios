//
//  LeasesViewController.swift
//  Automo
//
//  Created by OG Eric on 5/21/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class LeasesViewController: ButtonBarPagerTabStripViewController {

    override func viewDidLoad() {
        
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = .white
        settings.style.selectedBarHeight = 2.0
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RequestsViewController") as! RequestsViewController
        vc.pageTitle = "REQUESTS"
        let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "RequestsViewController") as! RequestsViewController
        vc1.pageTitle = "LEASES"
        
        return [vc, vc1]
    }
}
