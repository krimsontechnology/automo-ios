//
//  requestTableViewCell.swift
//  Automo
//
//  Created by OG Eric on 5/2/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit

class RequestTableViewCell: UITableViewCell {
    
    @IBOutlet weak var requestMainImage: UIImageView!
    @IBOutlet weak var requestCarName: UILabel!
    @IBOutlet weak var requestTerm: UILabel!
    @IBOutlet weak var requestDate: UILabel!
    @IBOutlet weak var requestStatus: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
