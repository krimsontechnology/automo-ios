//
//  MainTabBarController.swift
//  Automo
//
//  Created by OG Eric on 4/22/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    var centerButton = UIButton()
    var currentIndex: Int = 0
    var wheelHidden: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.tintColor = .white
        self.changeWheelButton()
        
        NotificationCenter.default.addObserver(self, selector: #selector(changeWheelButton), name: NSNotification.Name.init(rawValue: "wheel"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc func showAddVehicleScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddVehicleNavController") as! UINavigationController
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func changeWheelButton() {
        if wheelHidden {
            let yAxis = self.view.frame.height > 810.0 ? self.view.frame.height - 84.5 : self.view.frame.height - 52.5
                
            self.centerButton = UIButton(frame: CGRect(x: (self.view.frame.width/2) - 27.5, y: yAxis, width: 55.0, height: 55.0))
            self.centerButton.imageView?.contentMode = .scaleToFill
            self.centerButton.imageView?.tintColor = UIColor.white.withAlphaComponent(0.5)
            self.centerButton.setImage(#imageLiteral(resourceName: "wheel"), for: .normal)
            self.centerButton.layer.cornerRadius = 22.5
            self.centerButton.addTarget(self, action: #selector(showAddVehicleScreen), for: .touchUpInside)
            self.view.insertSubview(self.centerButton, aboveSubview: self.tabBar)
            
            wheelHidden = false
        } else {
            self.centerButton.removeFromSuperview()
            wheelHidden = true
        }
    }

}
