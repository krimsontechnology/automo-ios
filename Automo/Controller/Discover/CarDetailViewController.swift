//
//  CarDetailViewController.swift
//  Automo
//
//  Created by OG Eric on 4/10/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import Hero

class CarDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var leaseButton: UIButton!
    
    var car = Car()
    var carSpecs = CarSpecifications()
    
    static var currentImageIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 70.0
        tableView.tableFooterView = UIView()
        
        self.downloadOwnerInfo()
        self.increaseViewCount()
        
        // Use this to remove the old instance of the car and bump it up
        if MAIN_USER.userRecentlyViewed.contains(car.carId!) {
            let viewed = MAIN_USER.userRecentlyViewed.filter { $0 != car.carId! }
            MAIN_USER.userRecentlyViewed = viewed
        }
        
        MAIN_USER.userRecentlyViewed.append(car.carId!)
        ref.child(DATABASE_USER).child(MAIN_USER.userId ?? "id").updateChildValues(MAIN_USER.toJSON())
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateImage), name: NSNotification.Name.init(rawValue: "images"), object: nil)
        
        leaseButton.isEnabled = MAIN_USER.userId != car.ownerId ? true : false
        leaseButton.alpha = MAIN_USER.userId != car.ownerId ? 1.0 : 0.5
        
    }
    
    @IBAction func backButton (sender: UIButton) {
        CarDetailViewController.currentImageIndex = 0
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func favoriteCar (sender: UIButton) {
        guard !MAIN_USER.userFavorites.contains(car.carId!) else { return }
        
        MAIN_USER.userFavorites.append(car.carId!)
        ref.child(DATABASE_USER).child(MAIN_USER.userId ?? "id").updateChildValues(MAIN_USER.toJSON())
        self.showErrorMessage(title: "Added To Favorites", message: "This vehicle has been added to your favorites. To see your favorites, click the profile tab and swipe over to the favorites page.")
    }
    
    func increaseViewCount() {
        ref.child(DATABASE_AUTOS).child(car.carId!).updateChildValues(["views": car.carViews + 1])
    }
    
    func downloadSpecifications() {
        // This URL should be downloaded whenever the lease is created and we should use the car specs url to get info
        Networking.getCarFromUrl(modelId: car.carModelId ?? "") { (specs) in
            if specs != nil {
                self.carSpecs = specs!
                self.tableView.reloadData()
            }
        }
    }
    
    func downloadOwnerInfo() {
        ref.child(DATABASE_USER).child(car.ownerId ?? "Unknown").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                if let thisUser = snapshot.value as? [String:Any] {
                    self.car.owner = User(JSON: thisUser)!
                }
                
                self.tableView.reloadData()
            }
        })
    }
    
    @objc func updateImage() {
        self.tableView.reloadData()
//        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
    }
    
    @IBAction func goToLeaseRequest(sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LeaseOptionsViewController") as! LeaseOptionsViewController
        vc.car = car
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension CarDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarDetailTableViewCell", for: indexPath) as! CarDetailTableViewCell
            cell.selectionStyle = .none
            if car.carImages.count > 0 {
                cell.carImage.sd_setImage(with: URL(string: car.carImages[CarDetailViewController.currentImageIndex]), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            } else {
                cell.carImage.sd_setImage(with: URL(string: car.carFeaturedImage ?? ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            }
            cell.carImage.hero.modifiers = [HeroModifier.cascade]
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarDetailTableViewCell2", for: indexPath) as! CarDetailTableViewCell
            cell.selectionStyle = .none
            cell.images = car.carImages
            cell.detailType = .image
            cell.collectionView.reloadData()
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarDetailTableViewCell3", for: indexPath) as! CarDetailTableViewCell
            cell.selectionStyle = .none
            cell.carName.text = self.configCarName(car: car)
            cell.carMonthlyRate.text = "$\(car.carPrice)/month"
            cell.carDetails.text = car.carDescription ?? "There is no description for this vehicle."
            cell.userFirstName.text = car.owner.userName ?? "User"
            cell.userImage.sd_setImage(with: URL(string: car.owner.userImageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "user"))
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarDetailTableViewCell5", for: indexPath) as! CarDetailTableViewCell
            cell.selectionStyle = .none
            cell.detailType = .facts
            cell.facts = [car.carColor ?? "", car.carCondition ?? "", "\(car.carMilage) mi.", car.carTransmission ?? "",car.carCreationDate ?? "" ,"\(car.carTerm) months", "\(car.carMilageAllowance) mi.", car.carLocation ?? ""]
            cell.collectionView.reloadData()
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarDetailTableViewCell4", for: indexPath) as! CarDetailTableViewCell
            cell.selectionStyle = .none
            cell.detailType = .features
            cell.features = car.carFeatures
            cell.collectionView.reloadData()
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarDetailTableViewCell6", for: indexPath) as! CarDetailTableViewCell
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        switch indexPath.row {
        case 0:
            return self.view.frame.height > 810 ? 250 : 220
        case 1:
            return 120.0
        case 2:
            return UITableView.automaticDimension
        case 3:
            return 265.0
        case 4:
            return 120.0
        default:
            return 45.0
        }
    }
}
