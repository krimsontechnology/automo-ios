//
//  CarDetailTableViewCell.swift
//  Automo
//
//  Created by OG Eric on 4/14/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit

enum DetailType {
    case image, features, facts
}

class CarDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var carImage: UIImageView! // Updates as photos are clicked
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userFirstName: UILabel!
    @IBOutlet weak var carName: UILabel! //Year, Make, Model, Trim
    @IBOutlet weak var carDetails: UILabel!
    @IBOutlet weak var carMonthlyRate: UILabel! //Price
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // Features, Car Specs and Images should each have their own individual collection view. Car description should be one singular page
    
    var images: [String] = []
    var features: [String] = []
    var facts: [String] = []
    var factNames: [String] = ["Color", "Condition", "Milage", "Transmission",
                               "Listed Date", "Minimum Lease Term", "Milage Allowed", "Location"]
    var detailType: DetailType = .image

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension CarDetailTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch detailType {
        case .image:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCollectionViewCell", for: indexPath) as! SearchCollectionViewCell
            cell.carImage.sd_setImage(with: URL(string: images[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            return cell
        case .features:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCollectionViewCell", for: indexPath) as! SearchCollectionViewCell
            cell.carImage.image = UIImage(named: "map")
            cell.carName.text = features[indexPath.row]
            return cell
        case .facts:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCollectionViewCell", for: indexPath) as! SearchCollectionViewCell
            cell.carName.text = factNames[indexPath.row]
            cell.monthlyPrice.text = facts[indexPath.row]
            return cell
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch detailType {
            case .image:
                return images.count
            case .features:
                return features.count
            case .facts:
                return facts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch detailType {
        case .image:
            return CGSize(width: 60.0, height: 60.0)
        case .features:
            return CGSize(width: 60.0, height: 70.0)
        case .facts:
            return CGSize(width: 140.0, height: 55.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        CarDetailViewController.currentImageIndex = indexPath.row
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "images"), object: nil)
    }
}
