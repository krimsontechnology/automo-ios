//
//  MainViewController.swift
//  Automo
//
//  Created by OG Eric on 4/8/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage
import Hero

class MainViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var cars = [Car]()
    var recentlyViewedCars = [Car]()
    var titles = ["Popular Listings"," New Listings In Your Area"]
    var chosenIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.hero.navigationAnimationType = .fade
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.clearHeroIds()
        self.downloadAutos()
    }
    
    func clearHeroIds() {
        if let cell = self.tableView.cellForRow(at: IndexPath(row: chosenIndex, section: 1)) as? CarShowcaseTableViewCell {
            cell.featuredCarImage.hero.id?.removeAll()
        }
    }
    
    func downloadAutos() {
        ref.child(DATABASE_AUTOS).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                
                self.cars.removeAll()
                self.recentlyViewedCars.removeAll()
                
                if let snaps = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snaps {
                        if let dict = snap.value as? [String: Any] {
                            
                            let car = Car(JSON: dict)!
                            guard car.carCurrentlyHidden == false && car.carCurrentlyLeased == false else { continue }
                            
                            self.cars.append(car)
                            
                            if MAIN_USER.userRecentlyViewed.contains(car.carId ?? "") {
                                self.recentlyViewedCars.append(car)
                            }
                            
                            //TODO: Use this when more cars are on the app.
                            
//                            else {
//                                self.cars.append(car)
//                            }
                        }
                    }
                    
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarShowcaseTableViewCell2", for: indexPath) as! CarShowcaseTableViewCell
            cell.cars = recentlyViewedCars
            cell.collectionView.reloadData()
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarShowcaseTableViewCell", for: indexPath) as! CarShowcaseTableViewCell
            
            let thisCar = cars[indexPath.row]
            
            cell.carName.text = "\(thisCar.carYear ?? "") \(thisCar.carMake ?? "") \(thisCar.carModel ?? "")"
            cell.monthlyPrice.text = "$\(thisCar.carPrice)/month"
            cell.featuredCarImage.sd_setImage(with: URL(string: thisCar.carFeaturedImage ?? ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            
            cell.selectionStyle = .none
            return cell
            
        default:
            return CarShowcaseTableViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
            case 0:
                return 1
            default:
                return cars.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
            case 0:
                return self.recentlyViewedCars.count > 0 ? 142.0 : 0
            default:
                return 200.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 && self.recentlyViewedCars.count == 0 {
            return 0
        }
        
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 15, y: 0, width: self.tableView.frame.width, height: 40))
        view.backgroundColor = .black
        let label = UILabel(frame: view.frame)
        
        label.text = titles[section]
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            let cell = self.tableView.cellForRow(at: indexPath) as! CarShowcaseTableViewCell
            
            cell.featuredCarImage.hero.id = "carImage"
            cell.featuredCarImage.hero.modifiers = [.translate()]
            chosenIndex = indexPath.row
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CarDetailViewController") as! CarDetailViewController
            vc.car = cars[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
