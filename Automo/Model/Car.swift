//
//  Car.swift
//  Automo
//
//  Created by OG Eric on 4/8/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import Foundation
import ObjectMapper

// All Car Data Models Are Found Here

class Car: NSObject, Mappable {
    
    var carId: String?
    var carModelId: String?
    var carActive: Bool?
    
    // Car Details
    var carYear: String?
    var carMake: String?
    var carModel: String?
    var carTrim: String?
    var carColor: String?
    var carMilage: Int = 0
    var carCondition: String?
    var carTransmission: String?
    
    var carName: String?
    var carDescription: String?
    var carFeatures: [String] = []
    var carCreationDate: String?
    var carViews: Int = 0
    
    // Lease Details
    var carDeposit: Int = 0
    var carPrice: Int = 0
    var carTerm: Int = 0
    var carMilageAllowance: Int = 0
    var carLocation: String?
    var carCurrentlyLeased: Bool = false
    var carCurrentlyHidden: Bool = false
    
    // Car Images
    var carFeaturedImage: String?
    var carImages: [String] = []
    
    // Put the "Owner" class here and join on the backend
    var owner = User()
    var ownerId: String?
    
    required init?(map: Map) {}
    
    override init() {
        super.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        carId <- map["id"]
        carModel <- map["model"]
        carActive <- map["active"]
        carMake <- map["make"]
        carYear <- map["year"]
        carTrim <- map["trim"]
        carColor <- map["color"]
        carName <- map["name"]
        carDescription <- map["description"]
        carTransmission <- map["transmission"]
        carFeatures <- map["features"]
        carLocation <- map["location"]
        carCurrentlyLeased <- map["leased"]
        carCurrentlyHidden <- map["hidden"]
        carModelId <- map["modelId"]
        carCondition <- map["condition"]
        carPrice <- map["price"]
        carDeposit <- map["deposit"] // Should think about making this a slider $0 - up to $1500
        carViews <- map["views"]
        carTerm <- map["term"] 
        carMilage <- map["milage"]
        carMilageAllowance <- map["milageAllowed"]
        carFeaturedImage <- map["image"]
        carCreationDate <- map["date"]
        carImages <- map["gallery"]
        ownerId <- map["ownerId"]
    }
}

class CarMake: NSObject, Mappable {
    var id: String?
    var displayName: String?
    
    required init?(map: Map) {}
    
    override init() {
        super.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["make_id"]
        displayName <- map["make_display"]
    }
}

class CarModel: NSObject, Mappable {
    var id: String?
    var displayName: String?
    
    required init?(map: Map) {}
    
    override init() {
        super.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["model_make_id"]
        displayName <- map["model_name"]
    }
}

class CarTrims: NSObject, Mappable {
    var id: String?
    var displayName: String?
    
    required init?(map: Map) {}
    
    override init() {
        super.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["model_id"]
        displayName <- map["model_trim"]
    }
}

class CarSpecifications: NSObject, Mappable {
    
    var bodyStyle: String?
    var cylinders: String?
    var liters: String?
    var maxSpeed: String?
    var fuelType: String?
    var driveType: String?
    var seats: String?
    var doors: String?
    var fuelCity: String?
    var fuelHighway: String?
    var fuelCombined: String?
    var transmissionType: String?
    
    required init?(map: Map) {}
    
    override init() {
        super.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        
        bodyStyle <- map["model_body"]
        cylinders <- map["model_engine_cyl"]
        liters <- map["model_engine_l"]
        maxSpeed <- map["model_top_speed_kph"]
        fuelType <- map["model_engine_fuel"]
        driveType <- map["model_drive"]
        seats <- map["model_seats"]
        doors <- map["model_doors"]
        fuelCity <- map["model_mpg_city"]
        fuelHighway <- map["model_mpg_hwy"]
        fuelCombined <- map["model_mpg_mixed"]
        transmissionType <- map["model_transmission_type"]
    }
}
