//
//  LeaseOptionsViewController.swift
//  Automo
//
//  Created by OG Eric on 5/3/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import Stripe
import Firebase
import Alamofire

class LeaseOptionsViewController: UIViewController, STPPaymentCardTextFieldDelegate {
    
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var carLeaseRate: UILabel!
    @IBOutlet weak var carDepositAmount: UILabel!
    @IBOutlet weak var carTrim: UILabel!
    @IBOutlet weak var milageLimit: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var firstPayment: UILabel!
    
    @IBOutlet weak var leaseTermField: UITextField!
    @IBOutlet weak var startDateField: UITextField!
    @IBOutlet weak var requestButton: UIButton!
    
    // Stripe Element
    @IBOutlet weak var paymentTextField: STPPaymentCardTextField!

    let pickerView = UIPickerView()
    let datePicker = UIDatePicker()
    let formatter = DateFormatter()
    var car = Car()
    var terms = [Int]()
    var paymentAdded: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var x = car.carTerm
        
        while x < 13 {
            terms.append(x)
            x = x + 1
        }
        
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
        self.leaseTermField.inputView = pickerView
        
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: datePicker.date)!
        self.datePicker.datePickerMode = .date
        self.datePicker.minimumDate = tomorrow
        self.formatter.dateFormat = "MMM d, yyyy"
        self.startDateField.text = self.formatter.string(from: tomorrow)
        self.startDateField.inputView = datePicker
        
        self.datePicker.addTarget(self, action: #selector(updateDate), for: .valueChanged)
        
        self.paymentTextField.backgroundColor = .clear
        self.paymentTextField.textColor = .white
        self.paymentTextField.placeholderColor = .lightGray
        self.paymentTextField.cursorColor = .white
        
        self.updateViews()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func becomeFirst(sender: UIButton) {
        switch sender.tag {
        case 0:
            self.leaseTermField.becomeFirstResponder()
        case 1:
            self.startDateField.becomeFirstResponder()
        default:
            return
        }
    }
    
    @IBAction func backButton(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func updateViews() {
        self.carImage.sd_setImage(with: URL(string: car.carFeaturedImage ?? ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        self.carTrim.text = "Car Trim: \(self.car.carTrim ?? "")"
        self.carName.text = self.configCarName(car: self.car)
        self.milageLimit.text = "Monthly Milage: \(self.car.carMilage)"
        
        self.carLeaseRate.text = "$\(self.car.carPrice)/m"
        self.carDepositAmount.text = "$\(self.car.carDeposit)"
        self.totalPrice.text = "$\((car.carPrice * car.carTerm) + car.carDeposit)"
        self.firstPayment.text = "$\(car.carPrice + car.carDeposit)"
        
        self.leaseTermField.text = "\(car.carTerm) months"
    }
    
    @objc func updateDate() {
        self.startDateField.text = self.formatter.string(from: self.datePicker.date)
    }
    
    func paymentCardTextFieldDidEndEditing(_ textField: STPPaymentCardTextField) {
        if textField.isValid {
            // Make sure that the card has been completed entered and change the button to enabled
            Networking.createTokenForCard(cardNumber: textField.cardNumber!, params: textField.cardParams) { (token) in
                if token != nil {
                    Networking.addCardToCustomer(token: token!) { (done) in
                        if done == true {
                            
                            // IF THIS USER DOES NOT HAVE A STRIPE ACCOUNT - CREATE ONE HERE
                            
                            
                            //Enable the button if the payment is
                            self.requestButton.isEnabled = true
                        }
                    }
                }
            }
        } else {
            textField.clear()
        }
    }
    
    @IBAction func sendRequest(sender: UIButton) {
        let id = ref.childByAutoId().debugDescription.components(separatedBy: "/").last!
        let messageDict = ["id": id, "carId": car.carId!,"carName": self.carName.text ?? "",
                           "senderId": MAIN_USER.userId!, "senderName": MAIN_USER.userName!,
                           "ownerId": self.car.ownerId!, "ownerName": self.car.owner.userName!, "term": leaseTermField.text!, "startDate": datePicker.date.debugDescription, "date": Date().debugDescription, "status": "Pending"]
        
        MAIN_USER.userRequests.append(id)
        car.owner.userRequests.append(id)
        
        ref.child(DATABASE_REQUESTS).child(id).updateChildValues(messageDict)
        ref.child(DATABASE_USER).child(MAIN_USER.userId!).updateChildValues(["requests" : MAIN_USER.userRequests])
        ref.child(DATABASE_USER).child(car.ownerId!).updateChildValues(["requests" : car.owner.userRequests])
        
        self.navigationController?.popToRootViewController(animated: true)
    }

}

extension LeaseOptionsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return terms.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let term = terms[row]
        
        if term == 1 {
            return "\(term) Month"
        } else {
            return "\(term) Months"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let term = terms[row]
        
        if term == 1 {
            self.leaseTermField.text = "\(term) Month"
        } else {
            self.leaseTermField.text = "\(term) Months"
        }
    }
}
