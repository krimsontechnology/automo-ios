//
//  ProfileTableViewController.swift
//  Automo
//
//  Created by OG Eric on 5/2/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit
import Firebase
import XLPagerTabStrip

class ProfileTableViewController: UIViewController, IndicatorInfoProvider {

    @IBOutlet weak var tableView: UITableView!
    
    var profileUser = User()
    var cars = [Car]()
    var reviews = [Review]()
    
    var pageTitle = "Garage"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.profileUser = UserProfileViewController.profileUser
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 90, right: 0)
        
        self.downloadAutos()
        self.downloadUserReviews()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.profileUser = UserProfileViewController.profileUser
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: pageTitle)
    }
    
    func downloadAutos() {
        ref.child(DATABASE_AUTOS).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                self.cars.removeAll()
                
                if let snaps = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snaps {
                        if let dict = snap.value as? [String: Any] {
                            let car = Car(JSON: dict)!
                            
                            switch self.pageTitle {
                                
                            case "Garage":
                                if self.profileUser.userGarage.contains(car.carId ?? "id") {
                                    self.cars.append(car)
                                }
                            default :
                                if self.profileUser.userFavorites.contains(car.carId ?? "id") {
                                    self.cars.append(car)
                                }
                                
                            }
                        }
                    }
                    
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func downloadUserReviews() {
        ref.child(DATABASE_USER).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                self.reviews.removeAll()
                
                if let snaps = snapshot.children.allObjects as? [DataSnapshot] {
                    for snap in snaps {
                        if let dict = snap.value as? [String: Any] {
                            self.reviews.append(Review(JSON: dict)!)
                        }
                    }
                    
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @objc func showOptions(sender: UIButton) {
        let activity = UIAlertController(title: "My Vehicle Options", message: nil, preferredStyle: .actionSheet)
        let editOption = UIAlertAction(title: "Edit Listing", style: .default) { (action) in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddVehicleNavController") as! UINavigationController
            EnterCarDetailsViewController.currentCar = self.cars[sender.tag]
            self.present(vc, animated: true, completion: nil)
        }
        activity.addAction(editOption)
        
        if cars[sender.tag].carCurrentlyHidden {
            let unhideOption = UIAlertAction(title: "Publish Listing", style: .default) { (action) in
                ref.child(DATABASE_AUTOS).child(self.cars[sender.tag].carId ?? "id").updateChildValues(["hidden": false])
                self.showErrorMessage(title: "Vehicle Published", message: "Your vehicle is now publically available for leasing!")
            }
            activity.addAction(unhideOption)
        } else {
            let hideOption = UIAlertAction(title: "Hide Listing", style: .default) { (action) in
                ref.child(DATABASE_AUTOS).child(self.cars[sender.tag].carId ?? "id").updateChildValues(["hidden": true])
                self.showErrorMessage(title: "Vehicle Hidden", message: "You have hidden this vehicle from our public listings, whenever you are ready to put it back on the market just click publish listing!")
            }
            activity.addAction(hideOption)
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        activity.addAction(cancelButton)
        self.present(activity, animated: true, completion: nil)
    }
}

extension ProfileTableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch pageTitle {
        case "Reviews":
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath) as! ReviewTableViewCell
            
            let review = reviews[indexPath.row]
            
            cell.reviewRating.rating = Double(review.reviewRating ?? 0)
            cell.reviewUserName.text = review.reviewUserName ?? "Unknown"
            cell.reviewText.text = review.reviewText ?? ""
            cell.reviewUserImage.sd_setImage(with: URL(string: review.reviewUserImageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "profile"))
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarShowcaseTableViewCell", for: indexPath) as! CarShowcaseTableViewCell
            
            let thisCar = cars[indexPath.row]
            
            cell.carName.text = "\(thisCar.carYear ?? "") \(thisCar.carMake ?? "") \(thisCar.carModel ?? "")"
            cell.monthlyPrice.text = "$\(thisCar.carPrice)/month"
            cell.featuredCarImage.sd_setImage(with: URL(string: thisCar.carFeaturedImage ?? ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
            cell.optionsButton.addTarget(self, action: #selector(showOptions(sender:)), for: .touchUpInside)
            cell.optionsButton.tag = indexPath.row
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard pageTitle != "Reviews" else { return reviews.count }
        
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard pageTitle != "Reviews" else { return }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CarDetailViewController") as! CarDetailViewController
        vc.car = cars[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
