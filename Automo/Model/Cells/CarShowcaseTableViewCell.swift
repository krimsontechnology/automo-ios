//
//  CarShowcaseTableViewCell.swift
//  Automo
//
//  Created by OG Eric on 4/10/18.
//  Copyright © 2018 Eric Townsend. All rights reserved.
//

import UIKit

class CarShowcaseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var featuredCarImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var userFirstName: UILabel!
    @IBOutlet weak var timeSinceCreated: UILabel!
    @IBOutlet weak var monthlyPrice: UILabel!
    @IBOutlet weak var optionsButton: UIButton!

    @IBOutlet weak var collectionView: UICollectionView!
    
    var cars = [Car]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if userImage != nil { userImage.layer.borderColor = UIColor.white.cgColor }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension CarShowcaseTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCollectionViewCell", for: indexPath) as! SearchCollectionViewCell
        
        let thisCar = cars[indexPath.row]
        cell.carImage.sd_setImage(with: URL(string: thisCar.carFeaturedImage ?? ""), placeholderImage: #imageLiteral(resourceName: "Placeholder"))
        cell.carName.text = "\(thisCar.carYear ?? "") \(thisCar.carMake ?? "") \(thisCar.carModel ?? "")"
        cell.monthlyPrice.text = "$\(thisCar.carPrice)/month"
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cars.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = self.collectionView.frame.height
        return CGSize(width: height, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CarDetailViewController") as! CarDetailViewController
        vc.car = cars[indexPath.row]
        UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
    }
}
